package com.ubolthip.week10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.printf("%s area: %.1f \n", rec.getName(), rec.calArea());
        System.out.printf("%s perimeter: %.1f \n", rec.getName(), rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.printf("%s area: %.1f \n", rec2.getName(), rec2.calArea());
        System.out.printf("%s perimeter: %.1f \n", rec2.getName(), rec2.calPerimeter());

        System.out.println();

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area: %.3f \n", circle1.getName(), circle1.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle1.getName(), circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area: %.3f \n", circle2.getName(), circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle2.getName(), circle2.calPerimeter());

        System.out.println();
        
        Triangle triangle1 = new Triangle(4, 4, 6);
        System.out.println(triangle1.toString());
        System.out.printf("%s area: %.3f \n", triangle1.getName(), triangle1.calArea());
        System.out.printf("%s perimeter: %.1f \n", triangle1.getName(), triangle1.calPerimeter());

        Triangle triangle2 = new Triangle(7, 8, 9);
        System.out.println(triangle2.toString());
        System.out.printf("%s area: %.3f \n", triangle2.getName(), triangle2.calArea());
        System.out.printf("%s perimeter: %.1f \n", triangle2.getName(), triangle2.calPerimeter());
    }
}
